const app= Vue.createApp({
    data(){
      return{
        counter: 10,
        name:'',
        lastname:'',
        confirmedName:'',
        justName:'',
      };
    },
    watch:{
      counter(value){
        if(value>50){
          const that=this;
          setTimeout(function(){
            that.counter=10;
          },500);   
        }
      }
    }
    ,
    computed:{
      fullname(){
        console.log('Running again...');
        if(this.name===''){
          return '';
        }
        return this.name + ' '+ 'Miljanov';
      }
    },
    methods:{
      //order of METHODS doesnt play a role
      confirmInput(){
        this.confirmedName=this.name;
      },
      submitForm(){
        alert('Submitted!');
      },
      setJustName(event){
        this.justName=event.target.value;
      },
      setName(event){
         this.name= event.target.value;
      },
      setLastName(event, nick){
        this.lastname= event.target.value + " " + nick; 
      },
      add(num){
        this.counter=this.counter+num ;
      },
      remove(num){
        this.counter=this.counter-num;
      },
      resetInput(){
        this.justName='';
      }
    }
});

app.mount('#events');